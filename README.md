# holoData provide signed data to smart contract


- Archiving scripts : bash [bin/uar][1]

  usage: 
```bash
url="https://workflowy.com/#/d15c669d50ac"
bash bin/uar $url
```


[1]: bin/uar

# act as a trustedOracle / Cache

all data accessed is "content addressed" cached, timestamped and signed


```
qSC -> req    -> [holoData] -> (holoweb)
    <- INBOX  <- [holoData] <- (holoweb)

    -> OUTBOX -> [holoData] -> (holoweb)
```
   

